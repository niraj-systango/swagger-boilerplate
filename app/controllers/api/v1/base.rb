# frozen_string_literal: true

module API
  module V1
    class Base < API::Base
      format :json
      formatter :json, Grape::Formatter::Jbuilder
      helpers API::V1::Helpers::AuthenticationHelpers

      rescue_from ActiveRecord::RecordNotFound do |e|
        error!('Record not found.', 404)
      end

      rescue_from ActiveRecord::InvalidForeignKey do |e|
        error!('Unprocessable entity.', 422)
      end

      rescue_from ArgumentError do |e|
        error!(e.message.remove("'"), 422)
      end

      before do
        error!('Unauthorized request.', 401) unless authorized
      end

      helpers do
        def authorized
          authorization_key = Base64.strict_decode64(request.headers['Authorization']) rescue false
          authorization_key == "#{Constant::API_CLIENT_ID}:#{Constant::API_CLIENT_SECRET}"
        end
      end

      version 'v1'

      mount API::V1::Users

    end
  end
end
